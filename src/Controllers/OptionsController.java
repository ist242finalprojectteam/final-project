/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Models.*;
import Views.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 *
 * @author Jakeg527
 */
public class OptionsController {
    OptionsModel optionsModel;
    OptionsView optionsView;
    
    OptionsController(OptionsModel optionsModel, OptionsView optionsView) {
        this.optionsModel = optionsModel;
        this.optionsView = optionsView;   
        
        this.optionsView.checkLogin(new checkLoginListener());
        this.optionsView.saveRegistration(new saveRegistrationListener());
    }
    class checkLoginListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           boolean match = optionsModel.checkLoginInformation(optionsView.getUsernameLogin(), optionsView.getPasswordLogin());
           if (match) {
               optionsView.setMessage("Username: "+optionsModel.getSessionUsername()+" Password: "+optionsModel.getSessionPassword());
               optionsView.resetFields();
               optionsView.setVisibleFalse();
           }
           else {
               optionsView.setMessage("Login failed, try again");
           }
        }
    }
    
    class saveRegistrationListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           boolean confirm = optionsModel.setRegistration(optionsView.getUsernameRegister(), optionsView.getPasswordRegister(), optionsView.getNameRegister());
           if (confirm) {
               optionsView.setMessage("Registration saved! You are ready to login.");
               optionsView.resetFields();
               optionsView.setVisibleFalse();
           }
           else {
               optionsView.setMessage("Registration failed. Try again.");
           }
        }
    }
}
