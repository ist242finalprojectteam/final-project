/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Views.*;
import Models.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Jakeg527
 */
public class OrganizationsController {
//    Organization[] organizations;
    
//    JTextArea newArea;
    
    AddOrganizationsView addOrgView;
    OrganizationsModel orgModel;
    SeeOrganizationsView seeOrgView;
    
    OptionsModel optionsModel;
    
    OrganizationsController(OrganizationsModel orgModel, AddOrganizationsView addOrgView, SeeOrganizationsView seeOrgView, OptionsModel optionsModel) {
        this.addOrgView = addOrgView;
        this.orgModel = orgModel;
        this.seeOrgView = seeOrgView;
        
        this.optionsModel = optionsModel;
        
        this.addOrgView.addOrganization(new AddOrg());
    }
    class AddOrg implements ActionListener {            
        public void actionPerformed(ActionEvent e)
        {            
//            Organization newOrg = new Organization(addOrgView.getOrganizationName(), addOrgView.getOrganizerName(), addOrgView.getContactInfo());
//            orgModel.addOrg(newOrg);
//            redrawOrgs();
            orgModel.setOrg(addOrgView.getOrganizerName(), addOrgView.getOrganizationName(), addOrgView.getContactInfo());
            addOrgView.clearFields();
        }
    }
//    public void redrawOrgs() {
//            seeOrgView.removeOrgs();
//            for (Organization org : orgModel.getOrgs()) {
//                JPanel newPane = new JPanel();
//                if(optionsModel.getShowOrganizationName()) {
//                    JLabel orgName = new JLabel("Organization Name: "+org.getOrganizationName());
//                    newPane.add(orgName);
//                }
//                if(optionsModel.getShowOrganizerName()) {
//                    JLabel organizerName = new JLabel("Organizer Name: "+org.getOrganizerName());
//                newPane.add(organizerName);
//                }
//                if(optionsModel.getShowContactInfo()) {
//                    JLabel contactInfo = new JLabel("Contact Info: "+org.getContactInfo());
//                    newPane.add(contactInfo);
//                }
//                seeOrgView.addOrg(newPane);
//            }
//    }
}
