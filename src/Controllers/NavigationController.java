/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Controllers.*;
import Models.*;
import Views.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

//The controller that adds all instances of other models and views

public class NavigationController {
    //JFrame that holds all views
    MasterView masterView;
    
    //Model that holds general information
    MasterModel masterModel;
    
    //Splash screen and options
    MainView mainView;
    
    //Navigation buttons
    NavView navView;
    
    //Options
    OptionsModel optionsModel;
    OptionsView optionsView;
    OptionsController optionsController;  
    
    //Organizations
    AddOrganizationsView addOrganizationsView;
    SeeOrganizationsView seeOrganizationsView;
    OrganizationsModel organizationsModel;
    OrganizationsController organizationsController;
    
    //Other views
    CreditsView creditsView;
    InstructionsView instructionsView;
    
    //Array that holds all views
    View[] allViews;
   
    
    public NavigationController(MasterModel masterModel, MasterView masterView) {
        
        this.masterView = masterView;
        this.masterModel = masterModel;
        
        
        //MVC for setting options
        optionsModel = new OptionsModel();        
        optionsView = new OptionsView();
        optionsController = new OptionsController(optionsModel, optionsView); 
        
        //Create first screen view and navigation buttons
        mainView = new MainView();
        navView = new NavView();
        
        //Add nav and first screen to master view Jframe
        this.masterView.addView(navView);
        this.masterView.addView(mainView);
        
        //Create other views for navigation
        creditsView = new CreditsView();
        instructionsView = new InstructionsView();
        organizationsModel = new OrganizationsModel();
        addOrganizationsView = new AddOrganizationsView();
        seeOrganizationsView = new SeeOrganizationsView();
        organizationsController = new OrganizationsController(organizationsModel, addOrganizationsView, seeOrganizationsView, optionsModel);
        
        //Set up action listeners for navigation
        navView.switchToOptions(new OptionsButtonListener());
        navView.switchToInstructions(new InstructionsListener());
        navView.switchToCredits(new CreditsListener());
        navView.switchToAddOrganizations(new AddOrganizationsListener());
        navView.switchToSeeOrganizations(new SeeOrganizationsListener());
        optionsView.switchToMaster(new MainViewButtonListener());
        
        allViews = new View[] {mainView, optionsView, creditsView, instructionsView, addOrganizationsView, seeOrganizationsView};
        
        
    }
    class OptionsButtonListener implements ActionListener {            
        public void actionPerformed(ActionEvent e)
        {            
            masterView.removeViews(allViews);
            masterView.addView(optionsView);
        }
    }     
    class MainViewButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            masterView.removeViews(allViews);
//            organizationsController.redrawOrgs();
            masterView.addView(mainView);
        }
    }
    class CreditsListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            masterView.removeViews(allViews);
            String date = masterModel.getDate();
            creditsView.addDate(date);
            masterView.addView(creditsView);
        }
    }
    class InstructionsListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            masterView.removeViews(allViews);
            masterView.addView(instructionsView);
        }
    }
    class AddOrganizationsListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            masterView.removeViews(allViews);
            masterView.addView(addOrganizationsView);
        }
    }
    class SeeOrganizationsListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            masterView.removeViews(allViews);
            masterView.addView(seeOrganizationsView);
        }
    }
}
