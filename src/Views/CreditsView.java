package Views;

import Models.*;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;


public class CreditsView extends View {
    JLabel credits;
    JLabel date;
    
    public CreditsView(){
        credits = new JLabel("<html>Credits:<br><br>George Pendleton<br>Jake Goodman<br>Neil Riley</html>");
        date = new JLabel();
        
        add(credits);
    }
    public void addDate(String date) {
        this.date.setText("Date"+date);
        add(this.date);
    }

}
