package Views;


import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

//JPanel of navigation buttons that is kept on all pages

public class NavView extends View{
    JButton options;
    JButton instructions;
    JButton credits;  
    JButton addOrganizations;  
    JButton seeOrganizations; 
    
    public NavView(){     
        options = new JButton("Options");
        instructions = new JButton("Instructions");
        credits = new JButton("Credits");
        addOrganizations = new JButton("Add Organizations");
        seeOrganizations = new JButton("See Organizations");

        add(options, BorderLayout.SOUTH);
        add(instructions, BorderLayout.SOUTH);
        add(credits, BorderLayout.SOUTH);  
        add(addOrganizations, BorderLayout.SOUTH);
        add(seeOrganizations, BorderLayout.SOUTH);
    }

    //Action Listeners created for each button
    public void switchToOptions(ActionListener al)  {    
       options.addActionListener(al);
    }
    public void switchToInstructions(ActionListener al)  {    
       instructions.addActionListener(al);
    }
    public void switchToCredits(ActionListener al)  {    
       credits.addActionListener(al);
    }
    public void switchToAddOrganizations(ActionListener al)  {    
       addOrganizations.addActionListener(al);
    }
    public void switchToSeeOrganizations(ActionListener al)  {    
       seeOrganizations.addActionListener(al);
    }
}
