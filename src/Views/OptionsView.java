package Views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import javax.swing.*;

//Displays all options

public class OptionsView extends View{
    //Create all fields and labels privately
    private JButton goBack;
    private JButton checkLogin;
    private JButton saveRegister;
    private JButton loginButton;
    private JButton registerButton;
    private JLabel title;
    private JLabel status;
    
    private JLabel usernameLoginLabel;
    private JTextField usernameLogin;
    private JLabel passwordLoginLabel;
    private JTextField passwordLogin;
    
    private JLabel usernameRegisterLabel;
    private JTextField usernameRegister;
    private JLabel passwordRegisterLabel;
    private JTextField passwordRegister;
    private JLabel nameRegisterLabel;
    private JTextField nameRegister;
        
    public OptionsView(){ 
        GridBagConstraints layoutConst = new GridBagConstraints();
        setLayout(new GridBagLayout());
        layoutConst.insets = new Insets(10, 10, 10, 10);
        
        title = new JLabel();
        status = new JLabel("<html><b>You are currently not logged in</b></html>");
        goBack = new JButton("Return to home");
        
        loginButton = new JButton("Go to Login Screen");
        registerButton = new JButton("Go to Registration Screen");
        
        
        usernameLoginLabel = new JLabel("Username: ");
        usernameLogin = new JTextField(15);
        
        passwordLoginLabel = new JLabel("Password: ");
        passwordLogin = new JTextField(15);
        
        usernameRegisterLabel = new JLabel("Username: ");
        usernameRegister = new JTextField(15);
        
        passwordRegisterLabel = new JLabel("Password: ");
        passwordRegister = new JTextField(15);
        
        nameRegisterLabel = new JLabel("Name: ");
        nameRegister = new JTextField(15);
        
        checkLogin = new JButton("Login");
        saveRegister = new JButton("Register");
          
        
        layoutConst.gridx = 0;
        layoutConst.gridy = 0;
        add(status, layoutConst);
        
        layoutConst.gridx = 0;
        layoutConst.gridy = 1;
        add(loginButton, layoutConst);
        
        layoutConst.gridx = 0;
        layoutConst.gridy = 2;
        add(registerButton, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 0;
        add(title, layoutConst);

        layoutConst.gridx = 1;
        layoutConst.gridy = 1;
        add(usernameLoginLabel, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 2;
        add(usernameLogin, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 3;
        add(passwordLoginLabel, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 4;
        add(passwordLogin, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 1;
        add(usernameRegisterLabel, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 2;
        add(usernameRegister, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 3;
        add(passwordRegisterLabel, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 4;
        add(passwordRegister, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 5;
        add(nameRegisterLabel, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 6;
        add(nameRegister, layoutConst);
        
        layoutConst.gridx = 0;
        layoutConst.gridy = 10;
        add(goBack, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 10;
        add(checkLogin, layoutConst);
        
        layoutConst.gridx = 1;
        layoutConst.gridy = 10;
        add(saveRegister, layoutConst);
       
        setVisibleFalse();
        
        
        
        loginButton.addActionListener(myListener);
        registerButton.addActionListener(myListener);
        
        
    }
    public void setVisibleFalse() {
        usernameLoginLabel.setVisible(false);
        usernameLogin.setVisible(false);
        passwordLoginLabel.setVisible(false);
        passwordLogin.setVisible(false);
        usernameRegisterLabel.setVisible(false);
        usernameRegister.setVisible(false);
        passwordRegisterLabel.setVisible(false);
        passwordRegister.setVisible(false);
        nameRegisterLabel.setVisible(false);
        nameRegister.setVisible(false);
        checkLogin.setVisible(false);
        saveRegister.setVisible(false);
    }
    public String getUsernameLogin() {
        return usernameLogin.getText();
    }
    public String getPasswordLogin() {
        return passwordLogin.getText();
    }
    public String getUsernameRegister() {
        return usernameRegister.getText();
    }
    public String getPasswordRegister() {
        return passwordRegister.getText();
    }
    public String getNameRegister() {
        return nameRegister.getText();
    }
    public void setMessage(String message) {
        status.setText("<html><b>"+message+"</b></html>");
    }
    
    public void resetFields() {
        usernameLogin.setText("");
        passwordLogin.setText("");
        usernameRegister.setText("");
        passwordRegister.setText("");
        nameRegister.setText("");
        title.setText("");
    }
    ActionListener myListener = new java.awt.event.ActionListener() {
    @Override
    public void actionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource() == loginButton) { 
           setVisibleFalse();
           title.setText("Login");
           usernameLoginLabel.setVisible(true);
           usernameLogin.setVisible(true);
           passwordLoginLabel.setVisible(true);
           passwordLogin.setVisible(true);
           checkLogin.setVisible(true);
        } 
        else if (evt.getSource() == registerButton) { 
            setVisibleFalse();
            title.setText("Register");
            usernameRegisterLabel.setVisible(true);
            usernameRegister.setVisible(true);
            passwordRegisterLabel.setVisible(true);
            passwordRegister.setVisible(true);
            nameRegisterLabel.setVisible(true);
            nameRegister.setVisible(true);
            saveRegister.setVisible(true);
        }
       
      }
    };
    //Action listeners
    public void switchToMaster(ActionListener al)  {    
       goBack.addActionListener(al);
    }
    public void checkLogin(ActionListener al)  {    
       checkLogin.addActionListener(al);
    }
    public void saveRegistration(ActionListener al)  {    
       saveRegister.addActionListener(al);
    }
}
            
