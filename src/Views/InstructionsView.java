package Views;

import Models.*;
import java.awt.event.ActionListener;
import javax.swing.*;


public class InstructionsView extends View {
    JLabel instructions;
    
    public InstructionsView(){
        instructions = new JLabel("Instructions");
        
        add(instructions);
    }
}
