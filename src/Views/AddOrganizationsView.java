/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import javax.swing.*;

public class AddOrganizationsView extends View {
    
    private JButton addOrganization;
    private JLabel confirmation;
    
    private JLabel organizationName;
    private JTextField organizationNameField;
    
    private JLabel organizerName;
    private JTextField organizerNameField;
    
    private JLabel contactInfo;
    private JTextField contactInfoField;
    
        
    public AddOrganizationsView(){
        GridBagConstraints layoutConst = new GridBagConstraints();
        layoutConst.insets = new Insets(10, 10, 10, 10);
        
        this.setLayout(new GridBagLayout());
       
        organizationName = new JLabel("Enter Organization Name");
        layoutConst.gridx = 0;
        layoutConst.gridy = 0;
        add(organizationName, layoutConst);
        
        organizationNameField = new JTextField(8);
        layoutConst.gridx = 1;
        layoutConst.gridy = 0;
        add(organizationNameField, layoutConst);
        
        organizerName = new JLabel("Enter Organizer's Name");
        layoutConst.gridx = 0;
        layoutConst.gridy = 1;
        add(organizerName, layoutConst);
        
        organizerNameField = new JTextField(8);
        layoutConst.gridx = 1;
        layoutConst.gridy = 1;
        add(organizerNameField, layoutConst);

        contactInfo = new JLabel("Enter Contact Info (Phone/Email)");
        layoutConst.gridx = 0;
        layoutConst.gridy = 2;
        add(contactInfo, layoutConst);

        
        contactInfoField = new JTextField(8);
        layoutConst.gridx = 1;
        layoutConst.gridy = 2;
        add(contactInfoField, layoutConst);
        
        addOrganization = new JButton("Add Organization");
        layoutConst.gridx = 0;
        layoutConst.gridy = 3;
        add(addOrganization, layoutConst);
        
        confirmation = new JLabel();
        layoutConst.gridx = 0;
        layoutConst.gridy = 4;
        add(confirmation, layoutConst);       
    }
    public String getOrganizationName() {
        return this.organizationNameField.getText();
    }
    public String getOrganizerName() {
        return this.organizerNameField.getText();
    }
    public String getContactInfo() {
        return this.contactInfoField.getText();
    }
    public void clearFields() {
        organizationNameField.setText("");
        organizerNameField.setText("");
        contactInfoField.setText("");
    }
    public void confirmationMessage() {
        confirmation.setText("Your organization has been added!");
    }
    //Action listeners
    public void addOrganization(ActionListener al)  {    
       addOrganization.addActionListener(al);
    }
}
