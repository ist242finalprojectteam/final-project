/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.ScrollPane;
import javax.swing.*;

public class SeeOrganizationsView extends View {
    private JLabel header;
    private JLabel orgHeadLabel;
    private JLabel orgNameLabel;
    private JLabel contactInfoLabel;
    private JTextField orgHeadField;
    private JTextField orgNameField;
    private JTextField contactInfoField;
    private JTextArea resultsArea;
    private ScrollPane scroller;
//    private JPanel container;
    
    public SeeOrganizationsView(){
        
        GridBagConstraints layoutConst = new GridBagConstraints();
        layoutConst.insets = new Insets(10, 10, 10, 10);
        
        this.setLayout(new GridBagLayout());
        
        header = new JLabel("See All Organizations");
        layoutConst.gridx = 0;
        layoutConst.gridy = 0;
        add(header, layoutConst);
        
        orgHeadLabel = new JLabel("Enter name of events organizer");
        layoutConst.gridx = 0;
        layoutConst.gridy = 1;
        add(orgHeadLabel, layoutConst);
        
        orgHeadField = new JTextField(30);
        layoutConst.gridx = 1;
        layoutConst.gridy = 1;
        add(orgHeadField, layoutConst);
        
//        container = new JPanel();
//        layoutConst.gridx = 0;
//        layoutConst.gridy = 3;
//        add(container, layoutConst);
//
//    }
//    public void addOrg(JPanel org) {
//        container.add(org);
//        revalidate();
//        repaint();
//    }
//    public void removeOrgs() {
//        container.removeAll();
//    }
    }
}