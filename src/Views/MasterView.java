package Views;

import Controllers.*;

import java.awt.*;
import javax.swing.*;

//JFrame that holds all views

public class MasterView extends JFrame {
    JPanel container;
    
    public MasterView(){
        super("Master View");
        container = new JPanel();

        this.setContentPane(container);

    }
    public void addView(View view) {
        container.add(view, BorderLayout.SOUTH);
        validate();
        repaint();
    }
    public void removeViews(View[] views) {
        for(View view: views) {
            container.remove(view);
        }
        validate();
        repaint();
    }  
    
}
