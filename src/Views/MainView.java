/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Views;

import Models.*;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

//Splash screen and options

public class MainView extends View {
    JButton splashButton;
    JLabel maxQueriesLabel;
    JLabel maxDistanceLabel;
    JLabel showOrganizationNameLabel;
    JLabel showOrganizerNameLabel;
    JLabel showContactInfoLabel;
    JLabel showEventsListLabel;
    
    JPanel options;
    JPanel splash;
    
    
    public MainView() {
        
        GridBagConstraints layoutConst = new GridBagConstraints();
        
        splashButton = new JButton(new ImageIcon("src/psu_move_img.jpg"));  
        splash = new JPanel();
        splash.add(splashButton);
        
        options = new JPanel();
        options.setLayout(new GridBagLayout());
        layoutConst.gridx = 0;
        layoutConst.gridy = 0;
        
        layoutConst.insets = new Insets(10, 10, 10, 10);
        layoutConst.gridx = 0;
        layoutConst.gridy = 0;
        maxDistanceLabel = new JLabel();
        options.add(maxDistanceLabel, layoutConst);
        
        layoutConst.gridx = 0;
        layoutConst.gridy = 1;
        maxQueriesLabel = new JLabel();
        options.add(maxQueriesLabel, layoutConst);
        
        layoutConst.gridx = 0;
        layoutConst.gridy = 2;
        showOrganizationNameLabel = new JLabel();
        options.add(showOrganizationNameLabel, layoutConst);
        
        layoutConst.gridx = 0;
        layoutConst.gridy = 3;
        showOrganizerNameLabel = new JLabel();
        options.add(showOrganizerNameLabel, layoutConst);
        
        layoutConst.gridx = 0;
        layoutConst.gridy = 4;
        showContactInfoLabel = new JLabel();
        options.add(showContactInfoLabel, layoutConst);
        
        layoutConst.gridx = 0;
        layoutConst.gridy = 5;
        showEventsListLabel = new JLabel();
        options.add(showEventsListLabel, layoutConst);
        
        this.add(splash);
        this.add(options);
    }
}
