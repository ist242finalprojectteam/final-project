/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

//Options from options screen saved here

public class OptionsModel {
    private String username;
    private String password;
    
    public OptionsModel() {
        //These are local session variables
        username = "";
        password = "";
    }

    public boolean setRegistration(String username, String password, String name) {
        //Take created username and store it in database
        return false;
    }
    public boolean checkLoginInformation(String username, String password) {
        //Check username and password against database to see if it matches
        //If correct, store local variables and return true
        this.username = username;
        this.password = password;
        
        return false;
    }
    public String getSessionUsername() {
        return username;
    }
    public String getSessionPassword() {
        return password;
    }
}
