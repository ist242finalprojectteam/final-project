import Views.*;
import Controllers.*;
import Models.*;

public class App {
    public static void main(String[] args){
        MasterView masterView = new MasterView();
        MasterModel masterModel = new MasterModel();
        NavigationController controller = new NavigationController(masterModel, masterView);
        
        masterView.setVisible(true);
        masterView.setSize(800,500);
    }
}
